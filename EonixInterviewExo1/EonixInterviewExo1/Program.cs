﻿using System;

namespace EonixInterviewExo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exo 1");
            Console.WriteLine("======");
            Console.WriteLine("Le spectateur entre dans la salle");
            Console.WriteLine("Les dresseurs présentent leur singe");
            Console.WriteLine();

            do
            {

                Trainer trainer1 = new Trainer();
                trainer1.TrainerName = "Max";
                trainer1.Monkey.AddTour(new Tour("Musique", "La samba"));
                trainer1.Monkey.AddTour(new Tour("Acrobatie", "Marche sur les mains"));

                Trainer trainer2 = new Trainer();
                trainer2.TrainerName = "Marc";
                trainer2.Monkey.AddTour(new Tour("Musique", "La salsa"));
                trainer2.Monkey.AddTour(new Tour("Acrobatie", "Danse sur la tête"));
                Spectator sp = new Spectator();
                int numberTrainer = 0;
                while (numberTrainer > 2 || numberTrainer < 1)
                {
                    Console.WriteLine("Choix des dresseurs");
                    Console.WriteLine("1 - Dresseur numero 1");
                    Console.WriteLine("2 - Dresseur numero 2");
                    Console.Write(">");

                    while (!int.TryParse(Console.ReadLine(), out numberTrainer))
                    {
                        Console.WriteLine("Vous devez entrer un nombre");
                    }
                }
                if (numberTrainer == 1)
                {
                    trainerChoice(trainer1, sp);
                }
                else if (numberTrainer == 2)
                {
                    trainerChoice(trainer2, sp);
                }
                Console.WriteLine("Appuyer sur n'importe quelle touche pour continuer ou escape pour quitter");
                Console.WriteLine();
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
           

        }

        public static void trainerChoice(Trainer trainer, Spectator sp)
        {
            Console.WriteLine();
            Console.WriteLine("Choix des tours pour le dresseur "+ trainer.TrainerName);
            int choiceTour = 0;
            for (int i = 0; i < trainer.Monkey.Tours.Count; i++)
            {
                Console.WriteLine(i + " - " + trainer.Monkey.Tours[i].TourName + " - " + trainer.Monkey.Tours[i].TourType);
            }
            Console.Write(">");
            while(!int.TryParse(Console.ReadLine(), out choiceTour))
            {
                Console.WriteLine("Vous devez entrer un nombre");
            }
            if (trainer.Monkey.Tours[choiceTour].TourType == "Musique")
            {
                sp.Whistles();
            }
            else
            {
                sp.Applauds(trainer.Monkey.Tours[choiceTour].TourName);
            }
        }
    }
}
