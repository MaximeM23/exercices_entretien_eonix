﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonixInterviewExo1
{
    public class Tour
    {
        public string TourType { get; set; }
        public string TourName { get; set; }

        public Tour(string tourType, string tourName)
        {
            this.TourType = tourType;
            this.TourName = tourName;
        }
    }
}
