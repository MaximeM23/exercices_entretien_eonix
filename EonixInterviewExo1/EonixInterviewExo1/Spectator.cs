﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonixInterviewExo1
{
    public class Spectator
    {
        public void Applauds(string tour)
        {
            Console.WriteLine("Le spectateur applaudit pendant le tour : "+ tour);
        }

        public void Whistles()
        {
            Console.WriteLine("Le spectateur siffle");
        }
    }
}
