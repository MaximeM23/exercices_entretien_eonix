﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonixInterviewExo1
{
    public class Trainer
    {
        public Monkey Monkey { get; set; }
        public string TrainerName { get; set; }
        public Trainer()
        {
            Monkey = new Monkey();
            TrainerName = "";
        }
    }
}
