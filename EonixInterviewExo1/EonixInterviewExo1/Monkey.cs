﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonixInterviewExo1
{
    public class Monkey
    {
        private List<Tour> _tours;

        public List<Tour> Tours
        {
            get { return _tours; }
        }

        public Monkey()
        {
            _tours = new List<Tour>();
        }

        public void AddTour(Tour tour)
        {
            _tours.Add(tour);
        }


        public void MonkeyAcrobat(int value)
        {
            Console.WriteLine("Le singe se met à faire le tour : " + Tours[value].TourName);
        }

        public void MonkeySong(int value)
        {
            Console.WriteLine("Le singe se met à danser sur la musique : " + Tours[value].TourName);
        }
    }
}
