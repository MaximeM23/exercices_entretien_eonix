﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public static class Mapper
    {
        public static DAL.Models.Person PersonDTOToPersonDAL(this DTO.Models.Person p)
        {
            if(p != null)
            {
                return new DAL.Models.Person()
                {
                    Id = p.Id,
                    Firstname = p.Firstname,
                    Lastname = p.Lastname,
                };
            }
            return null;            
        }
        public static DTO.Models.Person PersonDALToPersonDTO(this DAL.Models.Person p)
        {
            if(p != null)
            {
                return new DTO.Models.Person()
                {
                    Id = p.Id,
                    Firstname = p.Firstname,
                    Lastname = p.Lastname,
                };
            }
            return null;
        }
    }
}
