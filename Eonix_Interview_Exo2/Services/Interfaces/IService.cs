﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.Interfaces
{
    public interface IServices<T> : IGetAllByFilterServices<T>, IDeleteByIdServices<T>, IUpdateServices<T>, IGetByIdServices<T>, IInsertServices<T>
    {
    }

    public interface IGetAllByFilterServices<T>
    {
        public IEnumerable<T> GetPersonByFilter(string firstname, string lastname);
    }

    public interface IDeleteByIdServices<T>
    {
        public bool DeleteById(Guid id);
    }
    public interface IUpdateServices<T>
    {
        public bool Update(T newValue);
    }
    public interface IGetByIdServices<T>
    {
        public T GetById(Guid id);
    }

    public interface IInsertServices<T>
    {
        public Guid Insert(T value);
    }

}
