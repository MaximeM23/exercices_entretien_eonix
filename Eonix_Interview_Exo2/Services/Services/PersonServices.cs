﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Text;
using DTO.Interfaces;
using DAL.Interfaces;

namespace DTO.Services
{
    public class PersonServices : IPersonService
    {
        private IPersonRepository _personRepository;

        public PersonServices(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public bool DeleteById(Guid id)
        {
            return _personRepository.DeleteById(id);
        }

        public Person GetById(Guid id)
        {
            return _personRepository.GetById(id).PersonDALToPersonDTO();
        }

        public IEnumerable<Person> GetPersonByFilter(string firstname, string lastname)
        {
            foreach(var dt in _personRepository.GetPersonByFilter(firstname, lastname))
            {
                yield return dt.PersonDALToPersonDTO();
            }
        }

        public Guid Insert(Person value)
        {
            if ((value.Firstname.Length < 1) || (value.Lastname.Length < 1)) return Guid.Empty;
            return _personRepository.Insert(value.PersonDTOToPersonDAL());
        }

        public bool Update(Person newValue)
        {
            return _personRepository.Update(newValue.PersonDTOToPersonDAL());
        }
    }
}
