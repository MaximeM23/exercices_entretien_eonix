﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.Models
{
    public class Person
    {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
