﻿using DTO.Interfaces;
using DTO.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eonix_Interview_Exo2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Person person)
        {
            if ((person.Lastname == null)|| (person.Firstname == null)) return BadRequest("Une personne doit être composé d'un nom et d'un prénom");
            return Ok(_personService.Insert(person));
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            Person p = _personService.GetById(id);
            if (p == null) return Ok("Cet id n'est pas utilisé");
            return Ok(p);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Person person)
        {
            if ((person.Lastname == null) || (person.Firstname == null)) return BadRequest("Une personne doit être composé d'un nom et d'un prénom");
            return Ok(_personService.Update(person));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            return Ok(_personService.DeleteById(id));
        }

        [HttpGet("ByNames")]
        public IActionResult GetByName(string? firstname = "", string? lastname = "")
        {
            return Ok(_personService.GetPersonByFilter(firstname, lastname));
        }
    }
}
