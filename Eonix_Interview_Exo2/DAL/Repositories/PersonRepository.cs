﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        public bool DeleteById(Guid id)
        {
            using (var db = new DataContext())
            {
                Person tmpPerson = new Person() { Id = id };
                db.Persons.Attach(tmpPerson);
                db.Persons.Remove(tmpPerson);
                if (db.SaveChanges() > 0) return true;
                return false;
            }
        }

        public Person GetById(Guid id)
        {
            using (var db = new DataContext())
            {
                 return db.Persons.Find(id);
            }
        }

        public IEnumerable<Person> GetPersonByFilter(string firstname, string lastname)
        {
            List<Person> p = new List<Person>();
            using (var db = new DataContext())
            {
                foreach(var pers in db.Persons.Where(x => x.Firstname.ToLower().Contains(firstname) || x.Lastname.ToLower().Contains(lastname)))
                {
                    p.Add(pers);
                }
            }
            return p;
        }

        public Guid Insert(Person value)
        {
            using (var db = new DataContext())
            {
                db.Add(new Person()
                {
                    Firstname = value.Firstname,
                    Lastname = value.Lastname
                });
                db.SaveChanges();
            }
            return value.Id;
        }

        public bool Update(Person newValue)
        {
            using (var db = new DataContext())
            {
                db.Update(newValue);
                if(db.SaveChanges() > 0) return true;
                return false;
            }
        }
    }
}
