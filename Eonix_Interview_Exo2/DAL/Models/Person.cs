﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class Person
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(100,ErrorMessage = "Le prénom doit être compris entre 1 et 100 caractères !",MinimumLength = 1)]
        public string Firstname { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Le nom doit être compris entre 1 et 100 caractères !", MinimumLength = 1)]        
        public string Lastname { get; set; }
    }
}
