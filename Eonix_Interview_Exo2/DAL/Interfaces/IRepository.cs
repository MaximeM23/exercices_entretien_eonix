﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IRepository<T> : IGetAllByFilterRepository<T>, IDeleteByIdRepository<T>, IUpdateRepository<T>, IGetByIdRepository<T>, IInsertRepository<T>
    {
    }

    public interface IGetAllByFilterRepository<T>
    {
        public IEnumerable<T> GetPersonByFilter(string firstname, string lastname);
    }

    public interface IDeleteByIdRepository<T>
    {
        public bool DeleteById(Guid id);
    }
    public interface IUpdateRepository<T>
    {
        public bool Update(T newValue);
    }
    public interface IGetByIdRepository<T>
    {
        public T GetById(Guid id);
    }

    public interface IInsertRepository<T>
    {
        public Guid Insert(T value);
    }


}
